public class App {
    public static void main(String[] args) throws Exception {
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2.0, null);
        Circle circle3 = new Circle(3.0, "green");
        System.out.println(circle1.toString());
        System.out.println(circle2.toString());
        System.out.println(circle3.toString());
        System.out.println("Diện tích circle1 là "+ circle1.getArea());
        System.out.println("Diện tích circle2 là "+ circle2.getArea());
        System.out.println("Diện tích circle3 là "+ circle3.getArea());

        Cylinder cylinder1 = new Cylinder();
        Cylinder cylinder2 = new Cylinder(2.5);
        Cylinder cylinder3 = new Cylinder(3.5, "green");
        Cylinder cylinder4 = new Cylinder(3.5, "green", 1.5);
        System.out.println(cylinder1.toString());
        System.out.println(cylinder2.toString());
        System.out.println(cylinder3.toString());
        System.out.println(cylinder4.toString());
        System.out.println("Thể tích cylinder1 là "+ cylinder1.getVolume());
        System.out.println("Thể tích cylinder2 là "+ cylinder2.getVolume());
        System.out.println("Thể tích cylinder3 là "+ cylinder3.getVolume());
        System.out.println("Thể tích cylinder4 là "+ cylinder4.getVolume());
    }
}
