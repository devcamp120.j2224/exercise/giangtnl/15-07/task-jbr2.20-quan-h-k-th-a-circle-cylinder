public class Cylinder extends Circle {
    double height = 1.0;
    public Cylinder () {
        super();
    }
    public Cylinder(double radius) {
        this.radius = radius;
    }
    public Cylinder(double radius, String color) {
        this.radius = radius;
        this.color = color;
    }
    
    public Cylinder(double radius, String color, double height) {
        super(radius, color);
        this.height = height;
    }
    public double getHeight() {
        return height;
    }
    public void setHeight(double height) {
        this.height = height;
    }
    public double getVolume(){
        return Math.PI*radius*2*height;
    }
    @Override
    public String toString(){
        return String.format("Cylinder [radius = %s, color = %s, height = %s]", radius, color, height);
    }
}
